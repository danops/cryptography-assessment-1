ATDOTHEYCALLYOUHEASKEDASHEACCO

I wrote a python function (CaesarShift.py - decrypt) that took a cipher text
and an offset and returned the decrypted letters. This function converts the
character into an ASCII value, subtracts 65 (so that A = 0, Z = 25), and then
performed the calculation (X - offset) % 26, which shifted the letters the
number of appropriate places. I then wrote a program that called this function
with offset values from 1 - 25 inclusive. After this, I used
GetMostLikelySolution.py, which looked at each plaintext to find the most
likely solution by counting the amount of words found in each plaintext. The
one with the most valid words in it was considered the most likely solution. 
