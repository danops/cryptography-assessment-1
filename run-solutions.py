while True:
    exerciseChoice = input("What exercise solution would you like to run? (Enter a number from 1-7) \n")
    if int(exerciseChoice) == 1:
        from src.exercise1 import solve
    elif int(exerciseChoice) == 2:
        from src.exercise2 import solve
    elif int(exerciseChoice) == 3:
        from src.exercise3 import solve
    elif int(exerciseChoice) == 4:
        from src.exercise4 import solve
    elif int(exerciseChoice) == 5:
        from src.exercise5 import solve
    elif int(exerciseChoice) == 6:
        from src.exercise6 import solve
    elif int(exerciseChoice) == 7:
        from src.exercise7 import solve
    else:
        print("That wasn't a valid choice! \n")
