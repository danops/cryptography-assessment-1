from unittest import TestCase

from src.tools.TextAnalysis import TextAnalysis

DICTIONARY = ["MEOW", "WOOF", "MOO"]


class TestTextAnalysis(TestCase):
    def setUp(self):
        super().setUp()
        self.analyser = TextAnalysis(DICTIONARY)

    def test_givenWordNotInDictionary_whenGettingWordCount_returnZero(self):
        self.assertEquals(0, self.analyser.getDictionaryWordCount(""))

    def test_givenOneOccurrenceOfWordInDictionary_whenGettingWordCount_returnOne(self):
        self.assertEquals(1, self.analyser.getDictionaryWordCount("MEOW"))

    def test_givenTwoOccurrencesOfFirstWordInDictionary_whenGettingWordCount_returnTwo(self):
        self.assertEquals(2, self.analyser.getDictionaryWordCount("MEOWMEOW"))

    def test_givenOneOccurrenceOfTwoWords_whenGettingWordCount_returnTwo(self):
        self.assertEquals(2, self.analyser.getDictionaryWordCount("MEOWWOOF"))

    def test_givenTwoOccurrencesOfOneWordAndOneOccurrenceOfAnother_whenGettingCount_returnThree(self):
        self.assertEquals(3, self.analyser.getDictionaryWordCount("MEOWMOOMEOW"))
