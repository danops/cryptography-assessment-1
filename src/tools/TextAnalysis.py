from typing import Set, List


class TextAnalysis:
    def __init__(self, dictionary: Set[str]):
        self.dictionary = dictionary

    def getDictionaryWordCount(self, text: str):
        count = 0
        for word in self.dictionary:
            count += text.count(word)
        return count

    def getMostOccurringWordsInText(self, text: List[str]):
        counter = {}
        for word in text:
            try:
                counter[word] += 1
            except KeyError:
                counter[word] = 1
        return sorted(counter.items(), key=lambda t: t[1], reverse=True)

    def getRepeatedWordsOccurrences(self, text: List[str]):
        repeatingWords = {}
        for i in range(len(text)- 1):
            if text[i] == text[i+1]:
                try:
                    repeatingWords[text[i]] += 1
                except KeyError:
                    repeatingWords[text[i]] = 1
        return sorted(repeatingWords.items(), key=lambda t: t[1], reverse=True)

    def getCharacterPercentagesInText(self, text: str):
        counter = {}
        for char in text:
            try:
                counter[char] += 1
            except KeyError:
                counter[char] = 1
        percentages = {}
        for char in counter:
            percentages[char] = (counter[char] / len(text)) * 100
        return sorted(percentages.items(), key=lambda t: t[1], reverse=True)

    def getDoubleLetterPatterns(self, text):
        doubleLetters = []
        for i in range(len(text) - 1):
            if(text[i] == text[i+1]):
                doubleLetters.append(text[i] + text[i+1])
        return set(doubleLetters)

    def getThreeCharacterPatternsInText(self, text):
        patterns = {}
        for i in range(len(text)):
            sequence = text[i:i + 3]
            try:
                patterns[sequence][1] += 1
                patterns[sequence][0].append(i)
            except KeyError:
                patterns[sequence] = [[i], 1]
        patterns = sorted(patterns.items(), key=lambda t: t[1][1], reverse=True)
        return patterns