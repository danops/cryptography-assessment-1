from typing import Set

from src.tools.TextAnalysis import TextAnalysis


class GetMostLikelySolution:
    def __init__(self, dictionary: Set[str]):
        self.analyser = TextAnalysis(dictionary)
        self.solutions = {}

    def addSolution(self, plainText):
        self.solutions[plainText] = self.analyser.getDictionaryWordCount(plainText)

    def execute(self, numberToFind: int):
        sortedSolutions = sorted(self.solutions.items(), key=lambda t: t[1], reverse=True)
        return sortedSolutions[:numberToFind]
