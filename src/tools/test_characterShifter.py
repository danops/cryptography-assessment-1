from unittest import TestCase

from src.tools.CharacterShifter import CharacterShifter


class TestCharacterShifter(TestCase):
    def setUp(self):
        super().setUp()
        self.shifter = CharacterShifter()

    def test_shiftCharacterBackwards(self):
        self.assertEquals("A", self.shifter.shiftCharacterBackwards("M", 12))
        self.assertEquals("Z", self.shifter.shiftCharacterBackwards("M", 13))
        self.assertEquals("A", self.shifter.shiftCharacterBackwards("A", 26))

    def test_shiftCharacterForwards(self):
        self.assertEquals("M", self.shifter.shiftCharacterForwards("A", 12))
        self.assertEquals("M", self.shifter.shiftCharacterForwards("Z", 13))
        self.assertEquals("A", self.shifter.shiftCharacterForwards("A", 26))

    def test_encryptCharacter(self):
        self.assertEquals("M", self.shifter.encryptCharacter("A", "M"))
        self.assertEquals("A", self.shifter.encryptCharacter("Z", "B"))
        self.assertEquals("A", self.shifter.encryptCharacter("A", "A"))

    def test_decryptCharacter(self):
        self.assertEquals("A", self.shifter.decryptCharacter("M", "M"))
        self.assertEquals("Z", self.shifter.decryptCharacter("A", "B"))
        self.assertEquals("A", self.shifter.decryptCharacter("A", "A"))
