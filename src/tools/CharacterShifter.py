ASCII_OFFSET=65

class CharacterShifter:
    def __init__(self):
        pass

    def shiftCharacterBackwards(self, character, offset):
        charIndex = ord(character) - ASCII_OFFSET
        shiftedIndex = (charIndex - offset) % 26
        return chr(shiftedIndex + ASCII_OFFSET)

    def shiftCharacterForwards(self, character, offset):
        return self.shiftCharacterBackwards(character, -offset)

    def encryptCharacter(self, plain_character, key_character):
        offset = ord(key_character) - ASCII_OFFSET
        return self.shiftCharacterForwards(plain_character, offset)

    def decryptCharacter(self, cipher_character, key_character):
        offset = ord(key_character) - ASCII_OFFSET
        return self.shiftCharacterBackwards(cipher_character, offset)
