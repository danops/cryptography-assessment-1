import itertools
import os

from src.decryption.CharacterAnalysis import ASCII_OFFSET, CharacterAnalysis
from src.decryption.VigenereCipher import VigenereCipher
from src.tools.GetMostLikelySolution import GetMostLikelySolution

# Gets the value of the key shifting each character back by E
def offset_by_e(key):
    key_approximation = ""
    for char in key:
        e_index = ord('E') - ASCII_OFFSET
        char_index = ord(char) - ASCII_OFFSET
        key_index = (char_index - e_index) % 26
        key_character = chr(key_index + ASCII_OFFSET)
        key_approximation += key_character
    return key_approximation


full_path = os.path.realpath(__file__)
dir_name = os.path.dirname(full_path) + "/"
file = open(dir_name + 'db458.txt')
tess26 = open(dir_name + '../tess26.txt').read()
tess27 = open(dir_name + '../tess27.txt').read()
cipherText = file.read()
solver = VigenereCipher()
characterAnalysis = CharacterAnalysis()
tess27Dictionary = set(word for word in tess27.split("|") if len(word) >= 3)
solutionFinder = GetMostLikelySolution(tess27Dictionary)

# Splits the cipher text into columns based on the key length (6)
cipherSplitIntoColumns = VigenereCipher.splitCipherTextIntoBlocks(cipherText, 6)

# Gets the top three occurring characters as tuples in each column
topThreeCharsInColumns = []
for text in cipherSplitIntoColumns:
    topThreeCharsInColumns.append(characterAnalysis.getTopThreeOccurringCharactersInText(text))

# Gets a list of the top three characters, extracting them from the tuples
listOfTopThreeChars = []
for topThree in topThreeCharsInColumns:
    topThreeInCol = []
    for char in topThree:
        topThreeInCol.append(char[0])
    listOfTopThreeChars.append(topThreeInCol)

# Gets the cartesian product of the top three character array to get all the possible keys to brute force
possibleKeys = list(itertools.product(*listOfTopThreeChars))
for key in possibleKeys:
    plainText = solver.decrypt(cipherText, offset_by_e(key))[:30]
    solutionFinder.addSolution(plainText)

# Prints the most likely solutions
print(solutionFinder.execute(5))
