import itertools
import os

from src.decryption.CharacterAnalysis import ASCII_OFFSET, CharacterAnalysis
from src.decryption.VigenereCipher import VigenereCipher
from src.tools.GetMostLikelySolution import GetMostLikelySolution

# Gets the value of the key shifting each character back by E
def offsetByE(key):
    key_approximation = ""
    for char in key:
        e_index = ord('E') - ASCII_OFFSET
        char_index = ord(char) - ASCII_OFFSET
        key_index = (char_index - e_index) % 26
        key_character = chr(key_index + ASCII_OFFSET)
        key_approximation += key_character
    return key_approximation


full_path = os.path.realpath(__file__)
dir_name = os.path.dirname(full_path) + "/"
file = open(dir_name + 'db458.txt')
tess26 = open(dir_name + '../tess26.txt').read()
tess27 = open(dir_name + '../tess27.txt').read()
tess27Dictionary = set(word for word in tess27.split("|") if len(word) >= 3)
cipherText = file.read().strip("\n")
solver = VigenereCipher()
characterAnalysis = CharacterAnalysis()
solutionFinder = GetMostLikelySolution(tess27Dictionary)

# Gets the occurrences of 3 letter patterns in the cipher text and where they occur
patterns = {}
for i in range(len(cipherText)):
    sequence = cipherText[i:i+3]
    try:
        patterns[sequence][1] += 1
        patterns[sequence][0].append(i)
    except KeyError:
        patterns[sequence] = [[i], 1]
topThreePatterns = sorted(patterns.items(), key=lambda t: t[1][1], reverse=True)[:3]
# Gets the positions of the most occurring patterns
topPatternPositions = topThreePatterns[0][1][0]
# Gets the difference in position between each of the patterns
topPositionDifferences = []
for i in range(len(topPatternPositions) - 1):
    topPositionDifferences.append(topPatternPositions[i+1] - topPatternPositions[i])

# Gets the values from 4-8 which divide directly into the differences in positions
differenceDivisors = {}
for difference in topPositionDifferences:
    for i in range(4, 9):
        if difference % i == 0:
            try:
                differenceDivisors[difference].append(i)
            except KeyError:
                differenceDivisors[difference] = [i]

# Gets all the possible key lengths based on the values that divided into the differences
possibleKeyLengths = list(differenceDivisors.values())[0]
for divisors in differenceDivisors.values():
    possibleKeyLengths = list(set(possibleKeyLengths) & set(divisors))

# Goes over each possible key length that was found and goes through the same process as exercise 3
for key_length in possibleKeyLengths:
    cipherTextSplitIntoColumns = VigenereCipher.splitCipherTextIntoBlocks(cipherText, key_length)
    topThreeCharsInColumns = []
    for block in cipherTextSplitIntoColumns:
        topThreeCharsInColumns.append(characterAnalysis.getTopThreeOccurringCharactersInText(block))
    listOfTopThreeChars = []
    for topThree in topThreeCharsInColumns:
        topThreeInCol = []
        for char in topThree:
            topThreeInCol.append(char[0])
        listOfTopThreeChars.append(topThreeInCol)

    possibleKeys = list(itertools.product(*listOfTopThreeChars))

    for key in possibleKeys:
        plainText = solver.decrypt(cipherText, offsetByE(key))[:30]
        solutionFinder.addSolution(plainText)

print(solutionFinder.execute(3))