from src.tools.CharacterShifter import CharacterShifter

# Used to decrypt vigenere ciphers
class VigenereCipher:
    def __init__(self):
        self.shifter = CharacterShifter()

    # Splits text into blocks based on the key length
    @staticmethod
    def splitCipherTextIntoBlocks(cipher_text, key_length):
        cipherTextSplitIntoBlocks = []
        for i in range(key_length):
            cipherTextSplitIntoBlocks.append("")
        for i in range(len(cipher_text)):
            cipherTextSplitIntoBlocks[i % key_length] += cipher_text[i]
        return cipherTextSplitIntoBlocks

    # Extends the key to match the cipher text length, so that
    # each letter of the cipher text is associated with a letter of a key
    def extendKeyToMatchCipherTextLength(self, cipher_text, key):
        paddedKey = key
        while len(cipher_text) > len(paddedKey):
            paddedKey += key
        return paddedKey

    # Decrypts the given cipher text with the given key
    def decrypt(self, cipher_text, key):
        if len(cipher_text) == 0 or len(key) == 0:
            return cipher_text

        key = self.extendKeyToMatchCipherTextLength(cipher_text, key)
        plainText = ""

        for i in range(len(cipher_text)):
            plainText += self.shifter.decryptCharacter(cipher_text[i], key[i])

        return plainText
