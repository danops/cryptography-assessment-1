from unittest import TestCase

from src.decryption.CaesarShift import CaesarShift

# Tests for the CaesarShift decryption
class TestCaesarShift(TestCase):
    def setUp(self):
        self.cipherText = None
        self.offset = None
        self.shift = CaesarShift()

    def assertPlainText(self, expected_plain_text):
        self.assertEquals(expected_plain_text, self.shift.decrypt(self.cipherText, self.offset))

    def test_givenEmptyCipherTextAndNoOffset_whenDecrypting_returnEmptyString(self):
        self.cipherText = ""
        self.offset = 0
        self.assertPlainText("")

    def test_givenCipherTextWithNoOffset_whenDecrypting_returnCipherText(self):
        self.cipherText = "Meow"
        self.offset = 0
        self.assertPlainText("Meow")

    def test_givenCipherWithOffsetOfOne_whenDecrypting_returnPlainText(self):
        self.cipherText = "BCDEFG"
        self.offset = 1
        self.assertPlainText("ABCDEF")

    def test_givenCipherTextWithOffsetOfTwentyFive_whenDecrypting_returnPlainText(self):
        self.cipherText = "ZABCDE"
        self.offset = 25
        self.assertPlainText("ABCDEF")

    def test_givenCipherTextWithOffsetOfTwentySix_whenDecrypting_returnCipherText(self):
        self.cipherText = "HELLO"
        self.offset = 26
        self.assertPlainText("HELLO")