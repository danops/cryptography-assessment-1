from unittest import TestCase

from src.decryption.VigenereCipher import VigenereCipher

# Tests for the vigenere cipher decryption 
class TestVigenereCipher(TestCase):
    def setUp(self):
        self.cipherText = None
        self.key = None
        self.solver = VigenereCipher()

    def assertPlainText(self, expected_plain_text):
        self.assertEquals(expected_plain_text, self.solver.decrypt(self.cipherText, self.key))

    def test_givenNoCipherTextAndNoKey_whenDecrypting_returnEmptyText(self):
        self.cipherText = ""
        self.key = ""
        self.assertPlainText("")

    def test_givenCipherTextAndNoKey_whenDecrypting_returnCipherText(self):
        self.cipherText = "MEOW"
        self.key = ""
        self.assertPlainText("MEOW")

    def test_givenCipherTextAndKeyContainingOnlyA_whenDecrypting_returnCipherText(self):
        self.cipherText = "MEOW"
        self.key = "A"
        self.assertPlainText("MEOW")

    def test_givenCipherTextAndKeyContainingOnlyM_whenDecrypting_returnPlainText(self):
        self.cipherText = "MNOPQ"
        self.key = "M"
        self.assertPlainText("ABCDE")

    def test_givenCipherTextAndKeyContainingMultipleLetters_whenDecrypting_returnPlainText(self):
        self.cipherText = "MOQSUWMOQSUW"
        self.key = "MNOPQR"
        self.assertPlainText("ABCDEFABCDEF")

