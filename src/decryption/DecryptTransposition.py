# Used for decrypting transposition ciphers
class DecryptTransposition:
    # The default ordering, assuming 5 columns and reading from left to right
    DEFAULT_ORDERING = [1,2,3,4,5]

    def __init__(self):
        pass

    # Splits the cipher text into a number of columns, based on the key length
    def splitCipherTextIntoColumns(self, cipher_text, key_length):
        columns = []
        columnLength = int(len(cipher_text) / key_length)
        for i in range(0, len(cipher_text), columnLength):
            columns.append(cipher_text[i : i + columnLength])
        return columns

    # Decrypts the given cipher text, ordering the columns based on the column_ordering argument
    def decrypt(self, cipher_text, column_ordering=None):
        if not column_ordering:
            column_ordering = self.DEFAULT_ORDERING
        cipherTextColumns = self.splitCipherTextIntoColumns(cipher_text, len(column_ordering))
        longestCol = len(cipherTextColumns[0])
        plainText = ""
        sorted_cols = ["" for x in range(len(column_ordering))]
        for i in range (len(column_ordering)):
            sorted_cols[column_ordering[i] - 1] = cipherTextColumns[i]

        for i in range(longestCol):
            for j in range(len(sorted_cols)):
                plainText += sorted_cols[j][i]

        return plainText