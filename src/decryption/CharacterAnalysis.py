import collections

# The ASCII value of A, used when shifting characters
ASCII_OFFSET = 65

# This class is used to analyse the characters in multiple ways
class CharacterAnalysis:
    # Creates the initial dictionary containing A-Z so that the letters can be counted
    def setupCharacterArray(self):
        self.character_count = collections.OrderedDict()
        for i in range(26):
            self.character_count[chr(i + ASCII_OFFSET)] = 0

    # Orders the characters in the character_count dictionary by the number of occurrences
    def orderByCharacterOccurrences(self):
        return collections.OrderedDict(
            sorted(self.character_count.items(), key=lambda t: t[1], reverse=True)
        )

    # Returns the count of the occurrences of each character in the given text, order by occurrences
    def analyse(self, text):
        self.setupCharacterArray()
        for char in text:
            self.character_count[char] += 1
        orderedByOccurrences = self.orderByCharacterOccurrences()
        return orderedByOccurrences

    # Gets the top three occurring characters in the given text
    def getTopThreeOccurringCharactersInText(self, text):
        analysis = self.analyse(text)
        return list(analysis.items())[:3]
