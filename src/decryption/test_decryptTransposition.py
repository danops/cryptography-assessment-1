from unittest import TestCase

from src.decryption.DecryptTransposition import DecryptTransposition

from src.encryption.EncryptTransposition import EncryptTransposition

# Tests for the transposition decryption
class TestDecryptTransposition(TestCase):
    def setUp(self):
        super().setUp()
        self.cipher_text = None
        self.column_ordering = None
        self.solver = DecryptTransposition()
        self.encryption = EncryptTransposition()

    def assertPlainText(self, plain_text):
        self.assertEquals(plain_text, self.solver.decrypt(self.cipher_text, self.column_ordering))

    def test_givenCipherTextWithColumnOrderingSameLengthAsText_returnCipherText(self):
        self.cipher_text = "ABCDEF"
        self.column_ordering = [1, 2, 3, 4, 5, 6]
        self.assertPlainText("ABCDEF")

    def test_givenCipherTextWithNumberOfColumnsSameAsCipherTextAndDifferentOrdering_returnPlainText(self):
        plainText = "ABCDEF"
        self.cipher_text = "FEDCBA"
        self.column_ordering = [6, 5, 4, 3, 2, 1]
        self.assertPlainText(plainText)

    def test_givenCipherTextWithColumnOrderingShorterThanTextAndOrderLeftToRight_returnPlainText(self):
        plainText = "ABCDEF"
        self.column_ordering = [1,2]
        self.cipher_text = self.encryption.encrypt(plainText, len(self.column_ordering))
        self.assertPlainText(plainText)

    def test_givenCipherTextWithColumnOrderingShortThanTextAndOrderRightToLeft_returnCorrectPlainText(self):
        plainText = "ABCDEF"
        self.cipher_text = "BDFACE"
        self.column_ordering = [2, 1]
        self.assertPlainText(plainText)