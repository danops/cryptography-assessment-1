from src.tools.CharacterShifter import CharacterShifter

class CaesarShift:
    def __init__(self):
        self.shifter = CharacterShifter()

    """
    Returns the cipher text when shifted by the value of the offset
    """
    def decrypt(self, cipher_text, offset):
        if not offset > 0:
            return cipher_text

        plainText = ""
        for char in cipher_text:
            plainText+= self.shifter.shiftCharacterBackwards(char, offset)

        return plainText
