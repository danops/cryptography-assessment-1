import csv
from src.tools.TextAnalysis import TextAnalysis
from src.tools.GetMostLikelySolution import GetMostLikelySolution
import os

full_path = os.path.realpath(__file__)
dir_name = os.path.dirname(full_path) + "/"
file = open(dir_name + 'db458.txt')
tess26 = open(dir_name + '../tess26.txt').read()
tess27 = open(dir_name + '../tess27.txt').read()
tess27Dictionary = set(word for word in tess27.split('|') if len(word) >= 3)
cipherText = file.read().strip("\n")
solutionFinder = GetMostLikelySolution(tess27Dictionary)
textAnalyser = TextAnalysis(tess27Dictionary)
# Reads the mapping csv to find which each character maps to
# The maps are lower case so that each character doesn't get replaced multiple times
mappings = {}
with open(dir_name + 'mappings.csv') as csvFile:
    mappingReader = csv.reader(csvFile, delimiter=",")
    for row in mappingReader:
        mappings[row[0]] = row[1]

# Goes through each character in the cipher text and replaces them with the mapped character
plainText = ""
for char in cipherText:
    plainText += mappings[char].upper()

print(plainText[:30])

