from src.decryption.CaesarShift import CaesarShift
from src.tools.GetMostLikelySolution import GetMostLikelySolution
import os

full_path = os.path.realpath(__file__)
dir_name = os.path.dirname(full_path) + "/"
file = open(dir_name + 'db458.txt')
tess26 = open(dir_name + '../tess26.txt').read()
tess27 = open(dir_name + '../tess27.txt').read()
tess27Dictionary = set(word for word in tess27.split("|") if len(word) >= 3)
cipherText = file.read()
solver = CaesarShift()
solutionFinder = GetMostLikelySolution(tess27Dictionary)

solutions = {}
# Decrypts the cipher text with every offset from 0-25
for i in range(26):
    plainText = solver.decrypt(cipherText, i)[:30]
    solutionFinder.addSolution(plainText)

# Prints the top 3 most likely solutions
print(solutionFinder.execute(3))