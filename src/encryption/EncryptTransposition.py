from src.encryption.TranspositionColumnOrdering import TranspositionColumnOrdering
from src.tools.CharacterShifter import CharacterShifter


class EncryptTransposition:
    def __init__(self):
        self.shifter = CharacterShifter()

    def splitTextIntoColumns(self, plain_text, key_length):
        columns = ["" for x in range(key_length)]
        for i in range(len(plain_text)):
            columns[i % key_length] += plain_text[i]
        return columns

    def encryptColumns(self, plain_text_columns, key):
        cipher_text_columns = []
        for i in range(len(plain_text_columns)):
            column_cipher = ""
            for j in range(len(plain_text_columns[i])):
                column_cipher += self.shifter.encryptCharacter(plain_text_columns[i][j], key[i % len(key)])
            cipher_text_columns.append(column_cipher)
        return cipher_text_columns

    def encrypt(self, plain_text, number_of_columns, column_ordering = TranspositionColumnOrdering.leftToRight):
        plainTextColumns = self.splitTextIntoColumns(plain_text, number_of_columns)
        cipher_text = ""

        if column_ordering == TranspositionColumnOrdering.leftToRight:
            for column in plainTextColumns:
                cipher_text += column
        return cipher_text
