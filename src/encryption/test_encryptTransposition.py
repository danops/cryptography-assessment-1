from unittest import TestCase

from encryption.TranspositionColumnOrdering import TranspositionColumnOrdering

from src.encryption.EncryptTransposition import EncryptTransposition


class TestEncryptTransposition(TestCase):
    def assertCipherText(self, cipher_text):
        self.assertEquals(cipher_text, self.encryption.encrypt(self.plainText, self.number_of_columns, self.columnOrder))

    def setUp(self):
        super(TestEncryptTransposition, self).setUp()
        self.plainText = None
        self.number_of_columns = None
        self.columnOrder = TranspositionColumnOrdering.leftToRight
        self.encryption = EncryptTransposition()

    def test_givenPlainTextAndKeyOfSameLength_returnCipherText(self):
        self.plainText = "ABCDEF"
        self.number_of_columns = 3
        self.assertCipherText("ADBECF")

    def test_givenPlainTextAndKeyOfShortedLengthAndColumnOrderLeftToRight_returnCipherText(self):
        self.plainText = "ABCDEF"
        self.number_of_columns = 2
        self.assertCipherText("ACEBDF")