from src.decryption.DecryptTransposition import DecryptTransposition
from src.tools.GetMostLikelySolution import GetMostLikelySolution
import os

full_path = os.path.realpath(__file__)
dir_name = os.path.dirname(full_path) + "/"
file = open(dir_name + 'db458.txt')
tess26 = open(dir_name + '../tess26.txt').read()
tess27 = open(dir_name + '../tess27.txt').read()
tess27Dictionary = set(word for word in tess27.split('|') if len(word) >= 3)
cipherText = file.read().strip("\n")
solver = DecryptTransposition()
solutionFinder = GetMostLikelySolution(tess27Dictionary)

# Tries column lengths from 4-8 to find the transposition cipher of each column amount
for i in range(4, 9):
    columnOrdering = [x for x in range(1, i + 1)]
    solutionFinder.addSolution(solver.decrypt(cipherText, columnOrdering)[:30])

print(solutionFinder.execute(3))