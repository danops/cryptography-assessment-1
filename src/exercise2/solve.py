from src.decryption.VigenereCipher import VigenereCipher
import os

full_path = os.path.realpath(__file__)
dir_name = os.path.dirname(full_path) + "/"
file = open(dir_name + 'db458.txt')
tess26 = open(dir_name + '../tess26.txt').read()
cipherText = file.read()
solver = VigenereCipher()

key = "TESSOFTHEDURBERVILLES"

# Gets the plaintext for the cipher text using the key above
plainText = solver.decrypt(cipherText, key)
print(plainText)