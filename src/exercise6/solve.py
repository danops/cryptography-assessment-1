from itertools import permutations

from src.decryption.DecryptTransposition import DecryptTransposition
from src.tools.GetMostLikelySolution import GetMostLikelySolution
import os

full_path = os.path.realpath(__file__)
dir_name = os.path.dirname(full_path) + "/"
file = open(dir_name + 'db458.txt')
tess26 = open(dir_name + '../tess26.txt').read()
tess27 = open(dir_name + '../tess27.txt').read()
tess27Dictionary = set(word for word in tess27.split('|') if len(word) >= 3)
cipherText = file.read().strip("\n")
solver = DecryptTransposition()
solutionFinder = GetMostLikelySolution(tess27Dictionary)

# Gets all the permutation of the array to find all the possible column orderings
possibleOrders = permutations([1, 2, 3, 4, 5, 6])
possibleOrdersList = []
for order in possibleOrders:
    possibleOrdersList.append(order)

# Goes over each possible order and decrypts the key with each ordering
for order in possibleOrdersList:
    solutionFinder.addSolution(solver.decrypt(cipherText, order)[:30])

print(solutionFinder.execute(3))
