E|DEAREST|TESSY|MINE|TOO|LATE|

The code for solving this is quite simple, I have a csv file (mappings.csv)
which contains the mappings of cipherText => plainText values, and my code
simply goes over every letter in the cipherText and replaces it with the
plainText value in the mapping. To work out these mappings I ran frequency
analysis on tess27 and the cipher text to find the most common letters. From
this I found that the pipe character was by far the most occurring character
in both, showing that the pipe was mapped onto the pipe. After this I found
the most second common letter was "J", meaning this was most likely "E".
After this point it was a lot of looking through the text for common
three/two letter combinations that were likely to be "TH", "THE", "AND",
etc and slowly filling out the mappings.